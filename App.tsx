import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import PokedexScreen from './screens/PokedexScreen';
import PokemonScreen from './screens/Pokemon/[name]';
import { NativeBaseProvider } from 'native-base';

const Stack = createStackNavigator();


export default function App() {

  return (
    <NativeBaseProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Pokedex'>
          <Stack.Screen name="Pokedex" component={PokedexScreen} />
          <Stack.Screen name="Pokemon" component={PokemonScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </NativeBaseProvider>
  );
}
