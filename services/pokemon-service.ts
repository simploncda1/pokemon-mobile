import axios from "axios";
import { Pokemon } from "../entities";

const API_URL = process.env.EXPO_PUBLIC_API_URL;

export async function fetchPokemonById(id : number){
    const response = await axios.get<Pokemon>(API_URL+'/api/pokemon/id/'+id);
    return response.data;
}


export async function fetchPokemonByName(nom : string){
    const response = await axios.get<Pokemon>(API_URL+'/api/pokemon/'+nom);
    return response.data;
}
