import axios from "axios";
import { Pokedex } from "../entities";

const API_URL = process.env.EXPO_PUBLIC_API_URL;

export async function fetchPokedex(){
    const response = await axios.get<Pokedex[]>(API_URL+'/api/pokedex');
    return response.data;
}