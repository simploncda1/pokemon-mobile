import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Pokemon } from '../../entities';
import { fetchPokemonByName } from '../../services/pokemon-service';
import PokemonDetail from '../../components/PokemonDetail';
import { Spinner } from 'native-base';



export default function PokemonScreen() {

    const route = useRoute();
    const { name }: any = route.params;

    const [pokemonData, setPokemonData] = useState<Pokemon>();

    useEffect(() => {
        fetchPokedexData();
    }, []);

    const fetchPokedexData = async () => {
        const data = await fetchPokemonByName(name);
        setPokemonData(data);
    };


    return (
        <View style={styles.container}>
            {pokemonData ? (
                <PokemonDetail pokemon={pokemonData} />
            ) : (
                <View style={styles.loadingContainer}>
                    <Spinner size="lg" />
                </View>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    loadingContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});
