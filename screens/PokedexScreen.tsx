import { FlatList, StyleSheet, View, Text } from 'react-native';
import React, { useEffect, useState } from 'react';
import { fetchPokedex } from '../services/pokedex-service';
import PokedexCard from '../components/PokedexCard';
import { Pokedex } from '../entities';

export default function PokedexScreen() {

  const [pokedexData, setPokedexData] = useState<Pokedex[]>([]);

  useEffect(() => {
    fetchPokedexData();
  }, []);

  const fetchPokedexData = async () => {
      const data = await fetchPokedex();
      setPokedexData(data);
  };


  const renderPage = ({ item }: any) => (
    <View style={styles.column}>
      <PokedexCard key={item.id} pokemon={item} />
    </View>
  );

  return (
    <FlatList
      data={pokedexData}
      renderItem={renderPage}
      keyExtractor={(item) => item.id.toString()}
      contentContainerStyle={styles.container}
      numColumns={2} // Affichez les éléments en colonnes de deux
    />
  );
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#B7B7B7',
    paddingVertical: 20,
  },
  column: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
});
