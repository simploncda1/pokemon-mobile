# Pokédex de la 2ème génération - React Native avec Expo

Ce projet est un Pokédex de la 2ème génération développé en utilisant React Native avec Expo. Il permet aux utilisateurs de consulter des informations détaillées sur chaque Pokémon, y compris des détails sur leur profil, leurs statistiques, leurs évolutions et leurs différentes formes.

## Comment lancer l'application

Pour lancer l'application, vous devez exécuter les commandes suivantes :

1. Assurez-vous d'avoir Node.js et npm installés sur votre machine.
2. Clonez ce dépôt sur votre machine.
3. Installez toutes les dépendances en exécutant la commande suivante dans le répertoire du projet :
    ```
    npm install
    ```
4. Lancez l'application en exécutant la commande :
    ```
    make serve
    ```

## Exécution des tests

Pour exécuter les tests, utilisez la commande suivante :
    ```
    make check
    ```

## Backend

Assurez-vous que le backend est lancé pour que l'application fonctionne correctement. Vous pouvez obtenir l'URL du backend [ici](https://gitlab.com/simplon30/pokemonback).

Si vous lancez le backend en local, vous pouvez utiliser `lt` (localtunnel) pour créer un tunnel et rendre votre serveur accessible depuis l'extérieur.

1. Assurez-vous que le backend est lancé sur le port 8000.

2. Utilisez la commande suivante pour lancer `lt` et créer un tunnel vers votre serveur local :
    ```
    make lt
    ```

## Maquettes

Vous trouverez ci-dessous les maquettes des différentes interfaces de l'application :

![Accueil](assets/maquette/HomeScreen.jpg)
![Détails](assets/maquette/DetailScreen.jpg)
![Formulaire](assets/maquette/FormScreen.jpg)
![Statistiques](assets/maquette/StatsScreen.jpg)
![Évolution](assets/maquette/EvolutionScreen.jpg)