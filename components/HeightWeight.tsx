import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const HeightWeight = ({ Height, Weight }: any) => {
    return (
        <View style={styles.container}>
            <View style={styles.infoContainer}>
                <View style={styles.iconContainer}>
                    <Text style={styles.label}>Taille</Text>
                </View>
                <Text style={styles.value}>{Number(Height) / 10} mètres</Text>
            </View>
            <View style={styles.infoContainer}>
                <View style={styles.iconContainer}>
                    <Text style={styles.label}>Poids</Text>
                </View>
                <Text style={styles.value}>{Number(Weight) / 10} kg</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        borderRadius: 15,
        height: 80,
        marginBottom: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 15,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    infoContainer: {
        alignItems: 'center',
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 2,
    },
    label: {
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 8,
    },
    value: {
        fontSize: 14,
        marginTop: 2,
    },
});

export default HeightWeight;
