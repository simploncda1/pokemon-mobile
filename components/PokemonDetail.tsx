import React, { useEffect, useState } from 'react';
import { Image, Text, TouchableOpacity, View, StyleSheet, ScrollView } from 'react-native';
import { Pokemon } from '../entities';
import { fetchPokemonById } from '../services/pokemon-service';
import FormesDetails from './details/FormesDetails';
import StatsDetails from './details/StatsDetails';
import EvolutionDetails from './details/EvolutionDetails';
import { useNavigation } from '@react-navigation/native';
import AProposDetails from './details/AProposDetails';
import { getTypeStyles } from '../utils/functions';


interface Props {
    pokemon: Pokemon
}
export default function PokemonDetail({ pokemon }: Props) {

    const typeStyle = getTypeStyles(pokemon.pokemon.type, "|");
    const formattedId = `#${String(pokemon.pokemon.id).padStart(3, '0')}`;
    const formattedType = pokemon.pokemon.type.split('|');

    const [selectedItem, setSelectedItem] = useState('A Propos');

    const handleItemClick = (item: any) => {
        setSelectedItem(item);
    };

    const [nextPkmn, setNextPkmn] = useState<Pokemon>();
    const [previousPkmn, setPreviousPkmn] = useState<Pokemon>();

    const navigation = useNavigation<any>();
    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: pokemon.pokemon.nom === '' ? 'No title' : pokemon.pokemon.nom,
        });
    }, [navigation, pokemon.pokemon.nom]);

    useEffect(() => {
        if (pokemon.pokemon.id) {
            fetchPokemonById(pokemon.pokemon.id + 1)
                .then(data => setNextPkmn(data))
            fetchPokemonById(pokemon.pokemon.id - 1)
                .then(data => setPreviousPkmn(data))
        }
    }, [])

    const handlePress = (name: string) => {
        navigation.replace('Pokemon', { name: name });
    };

    return (
        <ScrollView style={[styles.container, typeStyle]}>
            <View style={styles.textContainer}>
                <View style={styles.leftContent}>
                    <Text style={[styles.text, styles.nom]}>{pokemon.pokemon.nom}</Text>
                    {formattedType.map((type, index) => (
                        <Text key={index} style={styles.text}>{type}</Text>
                    ))}
                </View>
                <View style={styles.rightContent}>
                    <Text style={[styles.text, styles.nom]}>{formattedId}</Text>
                    <Text style={styles.text}>{pokemon.pokemon.genera}</Text>
                    <Text style={styles.text}>{pokemon.pokemon.shape}</Text>
                </View>
            </View>

            <View style={styles.imageContainer}>
                {previousPkmn ?
                    <TouchableOpacity onPress={() => handlePress(previousPkmn.pokemon.nom)}>
                        <Image
                            style={styles.imageNext}
                            resizeMode={'contain'}
                            source={{ uri: previousPkmn.pokemon.sprite }}
                        />
                    </TouchableOpacity>
                    :
                    <Image
                        style={styles.imageNext}
                        source={{ uri: "no-img" }}
                    />
                }
                <Image
                    style={styles.image}
                    resizeMode={'contain'}
                    source={{ uri: pokemon.pokemon.sprite }}
                />
                {nextPkmn ?
                    <TouchableOpacity onPress={() => handlePress(nextPkmn.pokemon.nom)}>
                        <Image
                            style={styles.imageNext}
                            resizeMode={'contain'}
                            source={{ uri: nextPkmn.pokemon.sprite }}
                        />
                    </TouchableOpacity>
                    :
                    <Image
                        style={styles.imageNext}
                        source={{ uri: "no-img" }}
                    />
                }
            </View>

            <View style={styles.info}>
                <View style={styles.menu}>
                    <TouchableOpacity
                        style={[styles.button, typeStyle]}
                        onPress={() => handleItemClick('A Propos')}
                    >
                        <Text style={styles.textButton}>A Propos</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.button, typeStyle]}
                        onPress={() => handleItemClick('Stats')}
                    >
                        <Text style={styles.textButton}>Stats</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.button, typeStyle]}
                        onPress={() => handleItemClick('Formes')}
                    >
                        <Text style={styles.textButton}>Formes</Text>
                    </TouchableOpacity>
                    {pokemon.evolution && pokemon.evolution.length > 0 &&
                        <TouchableOpacity
                            style={[styles.button, typeStyle]}
                            onPress={() => handleItemClick('Evolution')}
                        >
                            <Text style={styles.textButton}>Evolution</Text>
                        </TouchableOpacity>
                    }
                </View>


                {selectedItem === 'A Propos' &&
                    <AProposDetails pokemon={pokemon} />
                }

                {selectedItem === 'Stats' &&
                    <>
                        <StatsDetails pkmnStats={pokemon.pokemon.Hp} statsTxt={'PV'} />
                        <StatsDetails pkmnStats={pokemon.pokemon.Attack} statsTxt={'Attaque'} />
                        <StatsDetails pkmnStats={pokemon.pokemon.Defense} statsTxt={'Défense'} />
                        <StatsDetails pkmnStats={pokemon.pokemon.specialAttack} statsTxt={'Atq. Spé.'} />
                        <StatsDetails pkmnStats={pokemon.pokemon.specialDefense} statsTxt={'Déf. Spé.'} />
                        <StatsDetails pkmnStats={pokemon.pokemon.speed} statsTxt={'Vitesse'} />
                    </>
                }

                {selectedItem === 'Formes' &&
                    <FormesDetails pokemon={pokemon} />
                }

                {selectedItem === 'Evolution' && pokemon.evolution && pokemon.evolution.length > 0 &&
                    <EvolutionDetails pokemon={pokemon} />
                }
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    textContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    leftContent: {
        flex: 1,
    },
    rightContent: {
        flex: 1,
        alignItems: 'flex-end',
    },
    imageContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        height: 200,
        width: 200,
        marginVertical: 20,
        zIndex: 1,
    },
    imageNext: {
        height: 70,
        width: 70,
    },
    text: {
        color: 'white'
    },
    nom: {
        fontSize: 30,
    },
    info: {
        paddingHorizontal: 20,
        backgroundColor: 'white',
        zIndex: 2,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        // marginTop: -40,
        paddingBottom: 50,
    },
    menu: {
        marginTop: 30,
        marginBottom: 30,
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 40,
    },
    button: {
        width: '100%',
        marginVertical: 10,
        paddingVertical: 10,
        alignItems: 'center',
    },
    textButton: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
    },
});