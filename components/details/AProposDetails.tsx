import { Pokemon } from "../../entities"
import { Text, View, StyleSheet } from 'react-native';
import HeightWeight from "../HeightWeight";
import { genderRate } from "../../utils/functions";


interface Props {
    pokemon: Pokemon
}

export default function AProposDetails({ pokemon }: Props) {
    return (
        <View style={styles.container}>

            <Text style={styles.description}>{pokemon.pokemon.description}</Text>

            <HeightWeight Height={pokemon.pokemon.height} Weight={pokemon.pokemon.weight} />

            <Text style={styles.sectionTitle}>Talent :</Text>

            {pokemon.pokemon && pokemon.pokemon.abilities.split('|').map((ability, index) => (
                <Text key={index} style={styles.ability}>
                    {ability} : {pokemon.pokemon.abilitiesDesc.split('|')[index]}
                </Text>
            ))}

            <Text style={styles.sectionTitle}>Taux de Capture : </Text>
            <Text style={styles.ability}>{pokemon.pokemon.captureRate}%</Text>

            <Text style={styles.sectionTitle}>Sexe :</Text>
            <Text style={styles.ability}>{genderRate(pokemon.pokemon.genderRate)}</Text>

            <Text style={styles.sectionTitle}>Localisation :</Text>
            {pokemon.pokemon && pokemon.pokemon.encounters.split('|').map((encounter, index) => (
                <Text key={index} style={styles.encounter}>
                    {encounter}
                </Text>
            ))}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    description: {
        marginBottom: 10,
    },
    sectionTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    ability: {
        marginLeft: 10,
        marginBottom: 10,
    },
    encounter: {
        marginLeft: 10,
        marginBottom: 5,
    },
});
