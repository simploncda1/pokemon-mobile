import React from "react";
import { Image, Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Pokemon } from "../../entities";
import { useNavigation } from "@react-navigation/native";

interface Props {
    pokemon: Pokemon,
}

export default function EvolutionDetails({ pokemon }: Props) {
    const navigation = useNavigation<any>();
    const handlePress = (name: string) => {
        navigation.replace('Pokemon', { name: name });
    };

    return (
        <View>
            {pokemon.evolution && pokemon.evolution.map(pkmnEvo => (
                <View key={pkmnEvo.id}>
                    <View style={styles.rowContainer}>
                        <View style={styles.columnEvo}>
                            <TouchableOpacity onPress={() => handlePress(pkmnEvo.evolveFromName)}>
                                <>
                                    <Image style={styles.imageEvo} source={{ uri: pkmnEvo.evolveFromImg }} />
                                    <Text>{pkmnEvo.evolveFromName}</Text>
                                </>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.columnEvo}>
                            <View style={styles.columnEvo}>
                                <Text>{`->`}</Text>
                                {pkmnEvo.evolveToLvl && <Text>Lvl {pkmnEvo.evolveToLvl}</Text>}
                                {pkmnEvo.evolveToBonheur && <Text>Bonheur</Text>}
                                {pkmnEvo.evolveToBonheur && pkmnEvo.evolveToTimeOfDay && (
                                    <Text>
                                        {pkmnEvo.evolveToTimeOfDay.includes('day')
                                            ? 'Le jour'
                                            : pkmnEvo.evolveToTimeOfDay === 'night'
                                                ? 'La nuit'
                                                : null}
                                    </Text>
                                )}
                                <Text>
                                    {pkmnEvo.evolveToCondition &&
                                        (pkmnEvo.evolveToCondition.includes("Roche Royale|trade")
                                            ? "Échange + Roche Royale"
                                            : pkmnEvo.evolveToCondition.includes("Écaille Draco|trade")
                                                ? "Échange + Écaille Draco"
                                                : pkmnEvo.evolveToCondition.includes("Améliorator|trade")
                                                    ? "Échange + Améliorator"
                                                    : pkmnEvo.evolveToCondition === "trade"
                                                        ? "Échange"
                                                        : pkmnEvo.evolveToCondition === "level-up"
                                                            ? null
                                                            : pkmnEvo.evolveToCondition.includes("level-up|")
                                                                ? pkmnEvo.evolveToCondition.split("level-up|")[1]
                                                                : pkmnEvo.evolveToCondition)}
                                </Text>
                            </View>
                        </View>

                        <View style={styles.columnEvo}>
                            <TouchableOpacity onPress={() => handlePress(pkmnEvo.evolveToName)}>
                                <>
                                    <Image style={styles.imageEvo} source={{ uri: pkmnEvo.evolveToImage }} />
                                    <Text>{pkmnEvo.evolveToName}</Text>
                                </>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            ))}
        </View>
    )
}

const styles = StyleSheet.create({
    rowContainer: {
        flexDirection: 'row',
    },
    columnEvo: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    imageEvo: {
        width: 100,
        height: 100,
        resizeMode: 'cover',
    },
});
