import React from 'react';
import { Progress } from 'native-base';
import { Text, View, StyleSheet } from 'react-native';
import { statsColors } from '../../utils/functions';

interface Props {
    pkmnStats: number;
    statsTxt: string;
}

export default function StatsDetails({ pkmnStats, statsTxt }: Props) {
    return (
        <>
            <View style={styles.container}>
                <Text style={styles.label}>{statsTxt} :</Text>
                <Text style={styles.value}>{pkmnStats}</Text>
            </View>
            <Progress value={pkmnStats} max={255} _filledTrack={{ bg: statsColors(pkmnStats) }} />
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
        marginTop: 10,
    },
    label: {
        fontSize: 18,
        fontWeight: 'bold',
        marginRight: 10,
    },
    value: {
        fontSize: 18,
    },
});
