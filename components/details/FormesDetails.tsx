import React from "react";
import { Image, Text, View, StyleSheet } from 'react-native';
import { Pokemon } from "../../entities";

interface Props {
    pokemon: Pokemon,
}

export default function FormesDetails({ pokemon }: Props) {
    return (
        <View style={styles.container}>
            <View style={styles.formContainer}>
                <View style={styles.imageContainer}>
                    <Image
                        style={styles.image}
                        resizeMode={'contain'}
                        source={{ uri: pokemon.pokemon.sprite }}
                    />
                    <Text style={styles.text}>Normal</Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image
                        style={styles.image}
                        resizeMode={'contain'}
                        source={{ uri: pokemon.pokemon.spriteShiny }}
                    />
                    <Text style={styles.text}>Chromatique</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer: {
        flexDirection: 'row',
    },
    imageContainer: {
        alignItems: 'center',
    },
    image: {
        width: 150,
        height: 150,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16,
    }
});