import { Image, StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { Pokedex } from '../entities';
import { getTypeStyles } from '../utils/functions';

interface Props {
    pokemon: Pokedex
}

export default function PokedexCard({ pokemon }: Props) {

    const typeStyle = getTypeStyles(pokemon.type, ",");
    const formattedId = `#${String(pokemon.id).padStart(3, '0')}`;

    const navigation = useNavigation<any>();
    const handlePress = () => {
        navigation.navigate('Pokemon', { name: pokemon.nom });
    };

    return (
        <TouchableHighlight style={styles.container} onPress={handlePress}>
            <View style={[styles.container, typeStyle]}>
                <Text style={styles.id}>{formattedId}</Text>
                <Text style={styles.name}>{pokemon.nom}</Text>

                <Image
                    style={styles.image}
                    resizeMode={'contain'}
                    source={{ uri: pokemon.sprite }}
                />

                {pokemon.type.split(',').map((type, index) => (
                    <Text key={index} style={styles.types}>
                        {type}
                    </Text>
                ))}
            </View>
        </TouchableHighlight>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#bfbfbf',
        width: 130,
        height: 250,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 15,
    },
    id: {
    },
    name: {
        color: 'white',
        fontSize: 20,
    },
    types: {
        color: 'white',
        fontSize: 12,
        backgroundColor: 'grey',
        paddingHorizontal: 10,
        marginVertical: 5,
        borderRadius: 25,
    },
    image: {
        flex: 1,
        width: "100%",
        marginVertical: 20,
    },

});
