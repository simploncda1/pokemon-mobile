import { StyleSheet } from 'react-native';



export function genderRate(genderRate: number) {
    switch (genderRate) {
        case -1:
            return "Asexué";
        case 0:
            return "100% mâle";
        case 1:
            return "12.5% femelle ; 87.5% mâle";
        case 4:
            return "50% femelle ; 50% mâle";
        case 8:
            return "100% femelle";
        case 6:
            return "75 % femelle ; 25 % mâle";
        case 2:
            return "25 % femelle ; 75 % mâle";
        default:
            return "Taux de genre non reconnu";
    }
}

export const getTypeStyles = (type: string, separator: string) => {
    const typeArray = type.split(separator);
    const primaryType = typeArray[0].trim().toLowerCase();
    switch (primaryType) {
        case 'plante':
            return styles.bgPlante;
        case 'feu':
            return styles.bgFeu;
        case 'eau':
            return styles.bgEau;
        case 'normal':
            return styles.bgNormal;
        case 'électrik':
            return styles.bgElectrik;
        case 'insecte':
            return styles.bgInsecte;
        case 'roche':
            return styles.bgRoche;
        case 'poison':
            return styles.bgPoison;
        case 'fée':
            return styles.bgFee;
        case 'sol':
            return styles.bgSol;
        case 'spectre':
            return styles.bgSpectre;
        case 'psy':
            return styles.bgPsy;
        case 'acier':
            return styles.bgAcier;
        case 'combat':
            return styles.bgCombat;
        case 'glace':
            return styles.bgGlace;
        case 'ténèbres':
            return styles.bgTenebres;
        case 'dragon':
            return styles.bgDragon;
        default:
            return null;
    }
};

const styles = StyleSheet.create({
    bgPlante: {
        backgroundColor: '#58af94',
    },
    bgFeu: {
        backgroundColor: '#ea7571',
    },
    bgEau: {
        backgroundColor: '#75aef0',
    },
    bgNormal: {
        backgroundColor: '#bfbfbf',
    },
    bgElectrik: {
        backgroundColor: '#f7cd5c',
    },
    bgInsecte: {
        backgroundColor: '#a7bd1c',
    },
    bgRoche: {
        backgroundColor: '#a9756f',
    },
    bgPoison: {
        backgroundColor: '#8f6ba2',
    },
    bgFee: {
        backgroundColor: '#dda5c9',
    },
    bgSol: {
        backgroundColor: '#c7a9a4',
    },
    bgSpectre: {
        backgroundColor: '#65546d',
    },
    bgPsy: {
        backgroundColor: '#df608b',
    },
    bgAcier: {
        backgroundColor: '#6f707f',
    },
    bgCombat: {
        backgroundColor: '#dc6661',
    },
    bgGlace: {
        backgroundColor: '#9adadd',
    },
    bgTenebres: {
        backgroundColor: '#494949',
    },
    bgDragon: {
        backgroundColor: '#4b6edb',
    },
})


export function statsColors(pkmnStats: number) {
    let variant;

    if (pkmnStats < 70) {
        variant = 'error.500';
    } else if (pkmnStats < 100) {
        variant = 'yellow.500';
    } else {
        variant = 'green.500';
    }
    return variant;
}