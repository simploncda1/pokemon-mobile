import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PokedexScreen from '../screens/PokedexScreen';
import PokemonScreen from '../screens/Pokemon/[name]';
import { render } from '@testing-library/react-native';

const Stack = createStackNavigator();

// Mock du composant PokedexScreen et PokemonScreen
jest.mock('../screens/PokedexScreen', () => () => null);
jest.mock('../screens/Pokemon/[name]', () => () => null);

describe('<App />', () => {
  it('renders correctly', () => {
    const { getByText } = render(
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Pokedex'>
          <Stack.Screen name="Pokedex" component={PokedexScreen} />
          <Stack.Screen name="Pokemon" component={PokemonScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );

    // Vérifiez si le texte "Pokedex" est présent dans votre composant
    const pokedexText = getByText('Pokedex');
    expect(pokedexText).toBeTruthy();
  });
});
