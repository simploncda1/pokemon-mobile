import React from 'react';
import renderer from 'react-test-renderer';
import { TouchableHighlight } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import PokedexCard from '../../components/PokedexCard';
import { Germignon } from '../../utils/data';
// FIXME:
// Mocking useNavigation
jest.mock('@react-navigation/native', () => ({
  ...jest.requireActual('@react-navigation/native'),
  useNavigation: () => ({
    navigate: jest.fn(),
  }),
}));

describe('<PokedexCard />', () => {
  it('renders correctly with given props', () => {
    const pokemon = Germignon
    const tree = renderer
      .create(<PokedexCard pokemon={pokemon} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

//   it('navigates to Pokemon screen when pressed', () => {
//     const pokemon = {
//       id: 1,
//       nom: 'Pikachu',
//       type: 'Electric',
//       sprite: 'https://example.com/pikachu.png',
//     };
//     const navigationMock = useNavigation();
//     const component = renderer.create(<PokedexCard pokemon={pokemon} />);
//     const instance = component.root.findByType(TouchableHighlight);
//     instance.props.onPress();
//     expect(navigationMock.navigate).toHaveBeenCalledWith('Pokemon', { name: 'Pikachu' });
//   });
});
