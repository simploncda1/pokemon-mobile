import React from 'react';
import renderer from 'react-test-renderer';
import HeightWeight from '../../components/HeightWeight';

//TODO:
describe('<HeightWeight />', () => {
  it('renders correctly with given props', () => {
    const props = {
      Height: 180,
      Weight: 750,
    };
    const tree = renderer.create(<HeightWeight {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
