import React from 'react';
import { render, screen } from '@testing-library/react-native';
import EvolutionDetails from '../../../components/details/EvolutionDetails';

describe('<EvolutionDetails />', () => {
  it('renders evolution details correctly with various cases', () => {
    // Test case 1: No evolution data
    const pokemonNoEvolution : any = {
      evolution: []
    };
    render(<EvolutionDetails pokemon={pokemonNoEvolution} />);
    // Assert that no evolution details are rendered
    expect(screen.queryByText('->')).toBeNull();

    // Test case 2: Single evolution with level-up condition
    const pokemonSingleEvolutionLevelUp : any = {
      evolution: [
        {
          id: 1,
          evolveFromImg: 'evolveFromImg1',
          evolveFromName: 'EvolveFromName1',
          evolveToLvl: 20,
          evolveToName: 'EvolveToName1',
          evolveToImage: 'evolveToImage1'
        }
      ]
    };
    render(<EvolutionDetails pokemon={pokemonSingleEvolutionLevelUp} />);
    // Assert that evolution details are rendered correctly
    expect(screen.getByText('Lvl 20')).toBeTruthy();

    // Test case 3: Single evolution with trade condition
    const pokemonSingleEvolutionTrade : any = {
      evolution: [
        {
          id: 2,
          evolveFromImg: 'evolveFromImg2',
          evolveFromName: 'EvolveFromName2',
          evolveToName: 'EvolveToName2',
          evolveToImage: 'evolveToImage2',
          evolveToCondition: 'trade'
        }
      ]
    };
    render(<EvolutionDetails pokemon={pokemonSingleEvolutionTrade} />);
    // Assert that evolution details with trade condition are rendered correctly
    expect(screen.getByText('Échange')).toBeTruthy();

    // Test case 4: Single evolution with item condition
    const pokemonSingleEvolutionItem : any = {
      evolution: [
        {
          id: 3,
          evolveFromImg: 'evolveFromImg3',
          evolveFromName: 'EvolveFromName3',
          evolveToName: 'EvolveToName3',
          evolveToImage: 'evolveToImage3',
          evolveToCondition: 'Roche Royale'
        }
      ]
    };
    render(<EvolutionDetails pokemon={pokemonSingleEvolutionItem} />);
    // Assert that evolution details with item condition are rendered correctly
    expect(screen.getByText('Roche Royale')).toBeTruthy();

    // Add more test cases here for other scenarios

    // Test case 5: Multiple evolutions
    const pokemonMultipleEvolutions : any = {
      evolution: [
        {
          id: 4,
          evolveFromImg: 'evolveFromImg4',
          evolveFromName: 'EvolveFromName4',
          evolveToLvl: 20,
          evolveToName: 'EvolveToName4',
          evolveToImage: 'evolveToImage4'
        },
        {
          id: 5,
          evolveFromImg: 'evolveFromImg5',
          evolveFromName: 'EvolveFromName5',
          evolveToName: 'EvolveToName5',
          evolveToImage: 'evolveToImage5',
          evolveToCondition: 'trade'
        },
        {
          id: 6,
          evolveFromImg: 'evolveFromImg6',
          evolveFromName: 'EvolveFromName6',
          evolveToName: 'EvolveToName6',
          evolveToImage: 'evolveToImage6',
          evolveToCondition: 'Écaille Draco|trade'
        }
      ]
    };
    render(<EvolutionDetails pokemon={pokemonMultipleEvolutions} />);
    // Assert that multiple evolution details are rendered correctly
    expect(screen.getByText('Lvl 20')).toBeTruthy();
    expect(screen.getByText('Échange')).toBeTruthy();
    expect(screen.getByText('Échange + Écaille Draco')).toBeTruthy();
  });
});
