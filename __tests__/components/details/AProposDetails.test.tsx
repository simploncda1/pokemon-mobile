import React from 'react';
import { render } from '@testing-library/react-native';
import AProposDetails from '../../../components/details/AProposDetails';

describe('<AProposDetails />', () => {
  it('renders correctly with Pokemon details', () => {
    const mockPokemon : any = {
      pokemon: {
        description: 'Description du Pokémon',
        abilities: 'Talent 1|Talent 2',
        abilitiesDesc: 'Description talent 1|Description talent 2',
        captureRate: 20,
        genderRate: 0,
        encounters: 'Localisation 1|Localisation 2',
      },
    };

    const { getByText } = render(<AProposDetails pokemon={mockPokemon} />);

    // Vérifie si les éléments de détail du Pokémon sont rendus correctement
    expect(getByText('Description du Pokémon')).toBeTruthy();
    expect(getByText('Talent :')).toBeTruthy();
    expect(getByText('Talent 1 : Description talent 1')).toBeTruthy();
    expect(getByText('Talent 2 : Description talent 2')).toBeTruthy();
    expect(getByText('Taux de Capture :')).toBeTruthy();
    expect(getByText('20%')).toBeTruthy();
    expect(getByText('Sexe :')).toBeTruthy();
    expect(getByText('100% mâle')).toBeTruthy(); // Assurez-vous d'adapter le texte attendu en fonction de la valeur de genderRate
    expect(getByText('Localisation :')).toBeTruthy();
    expect(getByText('Localisation 1')).toBeTruthy();
    expect(getByText('Localisation 2')).toBeTruthy();
  });
});
