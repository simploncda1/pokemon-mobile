import { genderRate, getTypeStyles, statsColors } from "../../utils/functions";
import { StyleSheet } from 'react-native';

describe('genderRate function', () => {
    it('returns "Asexué" for gender rate -1', () => {
        expect(genderRate(-1)).toBe("Asexué");
    });

    it('returns "100% mâle" for gender rate 0', () => {
        expect(genderRate(0)).toBe("100% mâle");
    });

    it('returns "12.5% femelle ; 87.5% mâle" for gender rate 1', () => {
        expect(genderRate(1)).toBe("12.5% femelle ; 87.5% mâle");
    });

    it('returns "25 % femelle ; 75 % mâle" for gender rate 2', () => {
        expect(genderRate(2)).toBe("25 % femelle ; 75 % mâle");
    });

    it('returns "100% femelle" for gender rate 8', () => {
        expect(genderRate(8)).toBe("100% femelle");
    });

    it('returns "50% femelle ; 50% mâle" for gender rate 4', () => {
        expect(genderRate(4)).toBe("50% femelle ; 50% mâle");
    });

    it('returns "75 % femelle ; 25 % mâle" for gender rate 6', () => {
        expect(genderRate(6)).toBe("75 % femelle ; 25 % mâle");
    });

    it('returns "Taux de genre non reconnu" for unrecognized gender rate', () => {
        expect(genderRate(10)).toBe("Taux de genre non reconnu");
    });
});

// Mock des styles
const styles = StyleSheet.create({
    bgPlante: {
        backgroundColor: '#58af94',
    },
    bgFeu: {
        backgroundColor: '#ea7571',
    },
    bgEau: {
        backgroundColor: '#75aef0',
    },
    bgNormal: {
        backgroundColor: '#bfbfbf',
    },
    bgElectrik: {
        backgroundColor: '#f7cd5c',
    },
    bgInsecte: {
        backgroundColor: '#a7bd1c',
    },
    bgRoche: {
        backgroundColor: '#a9756f',
    },
    bgPoison: {
        backgroundColor: '#8f6ba2',
    },
    bgFee: {
        backgroundColor: '#dda5c9',
    },
    bgSol: {
        backgroundColor: '#c7a9a4',
    },
    bgSpectre: {
        backgroundColor: '#65546d',
    },
    bgPsy: {
        backgroundColor: '#df608b',
    },
    bgAcier: {
        backgroundColor: '#6f707f',
    },
    bgCombat: {
        backgroundColor: '#dc6661',
    },
    bgGlace: {
        backgroundColor: '#9adadd',
    },
    bgTenebres: {
        backgroundColor: '#494949',
    },
    bgDragon: {
        backgroundColor: '#4b6edb',
    },
})

describe('getTypeStyles function |', () => {
    it('returns correct style for "plante" type', () => {
        expect(getTypeStyles('plante', '|')).toStrictEqual(styles.bgPlante);
    });

    it('returns correct style for "feu" type', () => {
        expect(getTypeStyles('feu', '|')).toStrictEqual(styles.bgFeu);
    });

    it('returns correct style for "eau" type', () => {
        expect(getTypeStyles('eau', '|')).toStrictEqual(styles.bgEau);
    });

    it('returns correct style for "normal" type', () => {
        expect(getTypeStyles('normal', '|')).toStrictEqual(styles.bgNormal);
    });

    it('returns correct style for "électrik" type', () => {
        expect(getTypeStyles('électrik', '|')).toStrictEqual(styles.bgElectrik);
    });

    it('returns correct style for "insecte" type', () => {
        expect(getTypeStyles('insecte', '|')).toStrictEqual(styles.bgInsecte);
    });

    it('returns correct style for "roche" type', () => {
        expect(getTypeStyles('roche', '|')).toStrictEqual(styles.bgRoche);
    });

    it('returns correct style for "poison" type', () => {
        expect(getTypeStyles('poison', '|')).toStrictEqual(styles.bgPoison);
    });

    it('returns correct style for "fée" type', () => {
        expect(getTypeStyles('fée', '|')).toStrictEqual(styles.bgFee);
    });

    it('returns correct style for "sol" type', () => {
        expect(getTypeStyles('sol', '|')).toStrictEqual(styles.bgSol);
    });

    it('returns correct style for "spectre" type', () => {
        expect(getTypeStyles('spectre', '|')).toStrictEqual(styles.bgSpectre);
    });

    it('returns correct style for "psy" type', () => {
        expect(getTypeStyles('psy', '|')).toStrictEqual(styles.bgPsy);
    });

    it('returns correct style for "acier" type', () => {
        expect(getTypeStyles('acier', '|')).toStrictEqual(styles.bgAcier);
    });

    it('returns correct style for "combat" type', () => {
        expect(getTypeStyles('combat', '|')).toStrictEqual(styles.bgCombat);
    });

    it('returns correct style for "glace" type', () => {
        expect(getTypeStyles('glace', '|')).toStrictEqual(styles.bgGlace);
    });

    it('returns correct style for "ténèbres" type', () => {
        expect(getTypeStyles('ténèbres', '|')).toStrictEqual(styles.bgTenebres);
    });

    it('returns correct style for "dragon" type', () => {
        expect(getTypeStyles('dragon', '|')).toStrictEqual(styles.bgDragon);
    });

    it('returns null for unrecognized type', () => {
        expect(getTypeStyles('unknown', '|')).toBeNull();
    });
});

describe('getTypeStyles function ,', () => {
    it('returns correct style for "plante" type', () => {
        expect(getTypeStyles('plante', ',')).toStrictEqual(styles.bgPlante);
    });

    it('returns correct style for "feu" type', () => {
        expect(getTypeStyles('feu', ',')).toStrictEqual(styles.bgFeu);
    });

    it('returns correct style for "eau" type', () => {
        expect(getTypeStyles('eau', ',')).toStrictEqual(styles.bgEau);
    });

    it('returns correct style for "normal" type', () => {
        expect(getTypeStyles('normal', ',')).toStrictEqual(styles.bgNormal);
    });

    it('returns correct style for "électrik" type', () => {
        expect(getTypeStyles('électrik', ',')).toStrictEqual(styles.bgElectrik);
    });

    it('returns correct style for "insecte" type', () => {
        expect(getTypeStyles('insecte', ',')).toStrictEqual(styles.bgInsecte);
    });

    it('returns correct style for "roche" type', () => {
        expect(getTypeStyles('roche', ',')).toStrictEqual(styles.bgRoche);
    });

    it('returns correct style for "poison" type', () => {
        expect(getTypeStyles('poison', ',')).toStrictEqual(styles.bgPoison);
    });

    it('returns correct style for "fée" type', () => {
        expect(getTypeStyles('fée', ',')).toStrictEqual(styles.bgFee);
    });

    it('returns correct style for "sol" type', () => {
        expect(getTypeStyles('sol', ',')).toStrictEqual(styles.bgSol);
    });

    it('returns correct style for "spectre" type', () => {
        expect(getTypeStyles('spectre', ',')).toStrictEqual(styles.bgSpectre);
    });

    it('returns correct style for "psy" type', () => {
        expect(getTypeStyles('psy', ',')).toStrictEqual(styles.bgPsy);
    });

    it('returns correct style for "acier" type', () => {
        expect(getTypeStyles('acier', ',')).toStrictEqual(styles.bgAcier);
    });

    it('returns correct style for "combat" type', () => {
        expect(getTypeStyles('combat', ',')).toStrictEqual(styles.bgCombat);
    });

    it('returns correct style for "glace" type', () => {
        expect(getTypeStyles('glace', ',')).toStrictEqual(styles.bgGlace);
    });

    it('returns correct style for "ténèbres" type', () => {
        expect(getTypeStyles('ténèbres', ',')).toStrictEqual(styles.bgTenebres);
    });

    it('returns correct style for "dragon" type', () => {
        expect(getTypeStyles('dragon', ',')).toStrictEqual(styles.bgDragon);
    });

    it('returns null for unrecognized type', () => {
        expect(getTypeStyles('unknown', ',')).toBeNull();
    });
});


describe('statsColors function', () => {
    it('returns correct color for stats less than 70', () => {
        expect(statsColors(69)).toBe('error.500');
    });

    it('returns correct color for stats between 70 and 99', () => {
        expect(statsColors(80)).toBe('yellow.500');
    });

    it('returns correct color for stats equal to or greater than 100', () => {
        expect(statsColors(100)).toBe('green.500');
    });
});
